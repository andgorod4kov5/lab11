﻿#include <iostream>
#include <iomanip> 
#include "windows.h"
#include "time.h"

using namespace std;

int main()
{
    const int n = 30, m = 30;
    int a[n][m];
    int i, j;
    srand(time(NULL));
    cout << "a[n][m]:\n";
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++) {
            a[i][j] = rand() % (49) - 32;
            cout << a[i][j] << " ";
        }
        cout << endl;
    }

    int min = a[0][0];

    for(i=0;i<n;i++)
    {
      for (j = 0; j < m; j++)
      {
          if (a[i][j] < min)
          {
              min = a[i][j];
          }
      }
      cout << "Min: " << min << " ";
      min = a[i][j];
    }
}
